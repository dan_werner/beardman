
local loader = require("../lib/Advanced-Tiled-Loader/Loader")
local HC = require("../lib/HardonCollider")
local ts = 32

local debug = false
local drawColliders = true

loader.path = "Assets/Maps/"

TileMap = {}
TileMap.__index = TileMap
function TileMap:new(levelFile, onCollide)
  local instance = {
    map = loader.load(levelFile),
    collider = HC(100, onCollide)
  }

  instance.fg = instance.map.layers["fg"]

  setmetatable(instance, TileMap)
  instance.collider_tiles = instance:buildCollider()
  return instance
end

function TileMap:draw(x,y, scale, overlap) -- screen coordinate offset for cam
  self.map:autoDrawRange(x, y, scale, overlap)
  self.map:draw()
  if drawColliders then
    for index,shape in ipairs(self.collider_tiles) do
      shape:draw()
    end
  end
end

-- hack to draw the foreground parallax layer in front
function TileMap:drawFg()
  self.fg:draw()
end


function TileMap:buildCollider()
  local collider_tiles = {}
  local solid_layer = self.map.layers["solid"]
  if solid_layer then 
    for x,y,tile in solid_layer:iterate() do
      if tile and tile.properties.solid then
        local tx = (x)*ts
        local ty = (y)*ts
        local ctile
        if tile.properties.left_slope ~= nil and tile.properties.right_slope ~= nil then
          local left_slope = tonumber( tile.properties.left_slope )
          local right_slope = tonumber( tile.properties.right_slope )

          -- print (left_slope, right_slope)

          local x1 = tx
          local y1 = ty + ts

          local x2 = tx
          local y2 = ty + (ts * left_slope)

          local x3 = tx + ts          
          local y3 = ty + (ts * right_slope)

          local x4 = tx + ts
          local y4 = ty + ts    -- bottom right

          -- print (  x1, y1, x2, y2, x3, y3, x4, y4  )

          if left_slope == 1 then
            ctile = self.collider:addPolygon( x2, y2, x3, y3, x4, y4 )
          elseif right_slope == 1 then
            ctile = self.collider:addPolygon( x1, y1, x2, y2, x3, y3 )
          else
            ctile = self.collider:addPolygon( x1, y1, x2, y2, x3, y3, x4, y4 )
          end

          ctile.slope = true

          ctile.slopeValue = left_slope - right_slope
          -- []
        else
          ctile = self.collider:addRectangle(tx, ty, ts, ts)
        end
        ctile.type = "tile"
        self.collider:addToGroup("tiles", ctile)
        self.collider:setPassive(ctile)
        table.insert(collider_tiles, ctile)
      end
    end
  end

  --print ("Found the following solid tiles:")
  --for k,v in pairs(collider_tiles) do print (k,v) end

  return collider_tiles
end

return TileMap
