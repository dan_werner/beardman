local effects = {}

-- final fantasy style spiral - zoom in and spiral 
function effects.spiral(camera, s, e, inc)
  local co = coroutine.create(function ()
    for rot=s,e,inc do
      coroutine.yield()
      camera.rotation = rot
      camera.scale = 1+rot
    end
    for rot=e,s,-inc do
      coroutine.yield()
      camera.rotation = rot
      camera.scale = 1+rot
    end
  end)
  return co
end

return effects
