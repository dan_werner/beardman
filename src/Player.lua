local Animation = require("../src/Animation")

local debug = true
local walkspeed = 30
local runspeed = 60
local jumpAmount = -1000
local gravity = 50
local standingThreshold = 5

local max_y = 1000 -- max vertical speed

-- max speeds for various states
local max_speeds = {
  standing = 15, --fixes wtf when moved by something else
  walking = 300,
  running = 500,
  sliding = 500
}

Player = {}
Player.__index = Player
function Player:new(iMap, startx, starty, w, h, filename, mapCollider)

  local bodyCollider = mapCollider:addCircle(startx+(0.5*w), starty+(0.25*h), 0.2*h)
  local footCollider = mapCollider:addCircle(startx+(0.5*w), starty+(0.75*h), 0.2*h)

  local spriteSheet = love.graphics.newImage(filename)
  spriteSheet:setFilter("nearest", "nearest")

  local fs = loadFrames(spriteSheet, 40, 64)
  local anim = loadAnimations(fs)

  local instance = {
    inputMap = iMap,
    x = startx,
    y = starty,
    verticalState = "move_down",
    movementState = "standing",
    w = w,
    h = h,
    velocity = {
      x=0,
      y=0
    },
    frame = anim.stillImages["standing"],
    type = "player",
    bodyCollider = bodyCollider,
    footCollider = footCollider,
    mapCollider = mapCollider,
    spriteSheet = spriteSheet,
    animations = anim,
    frameSkip = 0
  }

  mapCollider:addToGroup("player", bodyCollider, footCollider)
  setmetatable(instance, Player)
  return instance
end

function Player:move(x,y)
  self.x = self.x + x
  self.y = self.y + y
  self.bodyCollider:move(x,y)
  self.footCollider:move(x,y)
end

function Player:handleInput()

  local left = self.inputMap["left"]
  local right = self.inputMap["right"]
  local down = self.inputMap["down"]
  local jump = self.inputMap["jump"]
  local run = self.inputMap["run"]
  local joystick = self.inputMap["joystick"]

  local accel = walkspeed
  -- sliding will override running or walking
  if love.keyboard.isDown(down) then
    if self.verticalState == "on_slope" then
      if self.shapeUnder ~= nil and self.shapeUnder.slope then
        self.movementState = "sliding"
      end
    elseif self.verticalState == "grounded" then
      -- duck?
    end
  elseif math.abs(self.velocity.x) > 0 then
    if love.keyboard.isDown(run) or
        (joystick and joystick:isDown(3)) then
      accel = runspeed
      self.movementState = "running"
    else
      self.movementState = "walking"
    end
  end


  if love.keyboard.isDown(left) or
      (joystick and joystick:isDown(12)) then
    self:accelerate(-(accel), 0)
  elseif love.keyboard.isDown(right) or
      (joystick and joystick:isDown(13)) then
    self:accelerate(accel, 0)
  end


  if not self.jumpLock then
    if love.keyboard.isDown(jump) or
        (joystick and joystick:isDown(1)) then
      self:doJump()
    end
  elseif self.jumpLock and
      (not love.keyboard.isDown(jump) and (not joystick or
          (joystick and not joystick:isDown(1)))) then
    self.jumpLock = false
  end

end

function Player:doJump()
  if self.verticalState == "grounded" then
    self:accelerate(0, jumpAmount)
    self.verticalState = "move_up"
    self.jumpLock = true
  end
end

function Player:accelerate(x,y)
  if math.abs(self.velocity.x) <= max_speeds[self.movementState] then
    self.velocity.x = self.velocity.x + x
  end

  if math.abs(self.velocity.y) <= max_y then
    self.velocity.y = self.velocity.y + y
  end
end


function Player:animate()
  -- update frames based on animation
  if self.movementState == "standing" then
    self.frame = self.animations.stillImages["standing"]
  elseif self.movementState == "walking" then
    if self.velocity.x > 0 then
      self.frame = self.animations.walkRight:nextFrame()
    else
      self.frame = self.animations.walkLeft:nextFrame()
    end
  elseif self.movementState == "running" then
    if self.velocity.x > 0 then
      self.frame = self.animations.runRight:nextFrame()
    else
      self.frame = self.animations.runLeft:nextFrame()
    end
  elseif self.movementState == "sliding" then
    if self.velocity.x > 0 then
      self.frame = self.animations.stillImages["ducking_right"]
    else
      self.frame = self.animations.stillImages["ducking_left"]
    end
  end
end

function Player:update(dt)

  self.shapeUnder = self:_shapeUnder()

  --if self.verticalState == "grounded" then

    if self.frameSkip == 3 then
      self:animate()
      self.frameSkip = 0
    else
      self.frameSkip = self.frameSkip + 1
    end

  -- this code was for pausing the animation when jumping... not sure that's best
  -- also affects going down slopes

  -- else
  --  if self.velocity.x > 10 then
  --    self.frame = self.animations.stillImages["jumping_right"]
  --  elseif self.velocity.x < -10 then
  --    self.frame = self.animations.stillImages["jumping_left"]
  --  else
  --   self.frame = self.animations.stillImages["standing"]
  --  end
  --end

  -- if we are grounded, we wont accelerate with gravity
  if self.verticalState == "grounded" then
    self:stop(false, true)
    if not self:isGrounded() then -- but we want to see if we are still grounded
      self.verticalState = "move_down"
    end
    if self.movementState == "sliding" then
      self.movementState = "standing"
    end
  elseif self.verticalState == "hit_ceiling" then
    self:stop(false, true)
  end

  -- when jumping, state is set to "move_up" - we correct automatically here
  if self.velocity.y > 0 and self:isGrounded() then
    self.verticalState = "move_down"
  end

  if self.shapeUnder ~= nil and
      self.shapeUnder.slope and
      math.abs(self.shapeUnder.slopeValue) > 0 then
    -- on a slope
    self.verticalState = "on_slope"
  end

  if self.verticalState == "on_slope" and self.movementState == "sliding" then
    if self.shapeUnder ~= nil and self.shapeUnder.slopeValue ~= nil then
      local slopeValue = self.shapeUnder.slopeValue
      print("sliding", self.shapeUnder.slopeValue)
      self:accelerate( -500 * slopeValue, gravity*slopeValue )
    else
      self:decayHorizontal()
    end
  end

  -- all states other than grounded accel down
  if self.verticalState ~= "grounded" then
    self:accelerate(0, gravity)
  end

  -- TODO: per-surface friction
  self:decayHorizontal()

  -- finally move the player itself
  self:move(self.velocity.x*dt, self.velocity.y*dt)
end


function Player:decayHorizontal()
  -- decay sideways inertia - keep with multiples of walkspeed
  if self.velocity.x > 0 then
    self.velocity.x = self.velocity.x - self.velocity.x * 0.1
  elseif self.velocity.x < 0 then
    self.velocity.x = self.velocity.x + math.abs(self.velocity.x) * 0.1
  end

  if math.abs(self.velocity.x) < standingThreshold then
    self.movementState = "standing"
    self.velocity.x = 0
  end
end

function Player:_shapeUnder()
  local shapes = self.mapCollider:shapesAt( self.x+(self.w/2), self.y+self.h+1 )
  return shapes[1]
end

-- Check two points beneath the player to see if they are still grounded
function Player:isGrounded()
  return self.shapeUnder ~= nil
end

-- stop on x, or y plane
function Player:stop(stop_x,stop_y)
  if stop_x then
    self.velocity.x = 0
  end
  if stop_y then
    self.velocity.y = 0
    self.verticalState = "grounded"
  end
end

function Player:draw(x, y) -- screen coordinates
  if self.frame then
    love.graphics.draw(self.spriteSheet, self.frame, x, y)
  end
  if debug then self.bodyCollider:draw() end -- may not reflect screen positions for now
  if debug then self.footCollider:draw() end -- may not reflect screen positions for now
end

function loadFrames(spriteSheet, frameWidth, frameHeight)
  local all_frames = {}
  for y = 0,spriteSheet:getHeight()/frameHeight-1,1 do
    local frameRow = {}
    for x = 0,spriteSheet:getWidth()/frameWidth-1,1 do
      -- crop out a frame from this row of the spriteSheet image
      local frame = love.graphics.newQuad(
        x*frameWidth,
        y*frameHeight,
        frameWidth,
        frameHeight,
        spriteSheet:getWidth(),
        spriteSheet:getHeight()
      )
      frameRow[x+1] = frame
    end
    -- add the row to the all_frames list
    all_frames[y+1] = frameRow
  end
  return all_frames
end

-- our frames are 40x64 on a 480x512 sheet
-- line 1 is still images
-- line 2 is running right, then running left, in groups of 6
-- line 3 is walking right, then walking left, in groups of 6
function loadAnimations(fs)
  local still_frames = {
    ["standing"] = fs[1][1],
    ["standing_away"] = fs[1][2],
    ["facing_left"] = fs[1][3],
    ["facing_right"] = fs[1][4],
    ["jumping_left"] = fs[2][7],
    ["jumping_right"] = fs[2][1],
    ["ducking_left"] = fs[1][10],
    ["ducking_right"] = fs[1][6]
  }

  -- would be nice if this were automated - chop out into for loop?

  local walking_right = getFrames(fs[3], 6, 0)
  local walking_left = getFrames(fs[3], 6, 6)

  local running_right = getFrames(fs[2], 6,0)
  local running_left = getFrames(fs[2], 6, 6)

  local animations = {
    stillImages = still_frames,
    runLeft   = Animation:new(running_left, true),
    runRight  = Animation:new(running_right, true),
    walkLeft  = Animation:new(walking_left, true),
    walkRight = Animation:new(walking_right, true)
  }

  return animations
end

function getFrames(row, count, offset)
  local target = {}
  for i=1, count,1 do
    target[i] = row[i+offset]
  end
  return target
end

return Player
