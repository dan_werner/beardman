Animation = {}
Animation.__index = Animation

local debugFrames = true

function Animation:new(frames, loop)
  local instance = {
    currentIndex = 1,
    numFrames = table.getn(frames),
    loop = loop,
    frames = frames
  }
  setmetatable(instance, Animation)
  return instance
end

function Animation:nextFrame()
  if self.currentIndex == self.numFrames then
    if self.loop then self.currentIndex = 1 end
  else
    self.currentIndex = self.currentIndex + 1
  end

  if debugFrames and self.currentFrame ~= nil then
    print("nextFrame:", table.getn(self.frames), self.currentIndex,  self.currentFrame:getViewport())
  end

  return self.frames[self.currentIndex]
end

function Animation:reset()
  self.currentIndex = 1
end

function Animation:getFrame(i)
  return self.frames[i]
end

return Animation
