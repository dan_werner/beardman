
Camera = {}
Camera.__index = Camera

function Camera:new()
  width, height = love.window.getDimensions()
  local instance = {
    width = width,
    height = height,
    x = 0,
    y = 0,
    rotation = 0,
    world_x = 0,
    world_y = 0,
    scale = 1
  }
  setmetatable(instance, Camera)
  return instance
end

function Camera:lookAt(x,y)
  local s = self.scale

  self.world_x = -x + self.width /2
  self.world_y = -y + self.height/2

  self.x = -x*s + (self.width/2)
  self.y = -y*s + (self.height/2)

  love.graphics.translate( self.width/2, self.height/2 )
  love.graphics.rotate( self.rotation )
  love.graphics.translate( -self.width/2, -self.height/2 )

  love.graphics.translate( self.x, self.y )
  love.graphics.scale( s, s )
end

return Camera

