
local Player = require("src/Player")
local TileMap = require("src/TileMap")
local Camera = require("src/Camera")
local effects = require("src/effects")

function love.conf(t)
  --t.window.vsync = true
  t.modules.joystick = true
  t.window.fullscreen=false
end

function love.load()
  loveframes = require("lib.LoveFrames")
  -- TODO:
  --  - option/pref system for settings like fullscreen, input etc
  --  - move level loading and initialization to after love.load, so the game can start and show a loading screen
  --
  --love.window.setFullscreen(true, "desktop")

  camera = Camera:new()
  joysticks = love.joystick.getJoysticks()
  level = TileMap:new("map3.tmx", onCollide)

  -- TODO: get player start position from tilemap
  local p1Controls = {
    ["left"] = "left",
    ["down"] = "down",
    ["right"] = "right",
    ["jump"] = "up",
    ["run"] = "shift",
    ["joystick"] = joysticks[1]
  }
  player = Player:new(p1Controls, 200, 100, 40, 64, "Assets/beardman_spritesheet.png", level.collider)

  local p2Controls = {
    ["left"]="a",
    ["right"]="d",
    ["down"] = "s",
    ["jump"] ="w",
    ["run"] = "v",
    ["joystick"] = joysticks[2]
  }
  player2 = Player:new(p2Controls, 100, 100, 40, 64, "Assets/beardman_spritesheet.png", level.collider)

  local debugGrid = loveframes.Create("grid")
  debugGrid:SetPos(5,25):SetColumns(2):SetRows(2):SetCellWidth(80)
  debugGrid:SetItemAutoSize(true)

  local label = loveframes.Create("button")
  label:SetSize(80,10):SetText("Horizontal state")
  horizontalState = loveframes.Create("button"):SetText("...")
  debugGrid:AddItem(label, 1, 1)
  debugGrid:AddItem(horizontalState, 1, 2)

  local label2 = loveframes.Create("button")
  label2:SetSize(80,10):SetText("Vertical state")
  verticalState = loveframes.Create("button"):SetText("...")
  debugGrid:AddItem(label2, 2, 1)
  debugGrid:AddItem(verticalState, 2, 2)
end

function onCollide(dt, shape, shape2, x, y)
  -- TODO: handle the other cases as well, perhaps offset balance
  if shape == player.footCollider or shape == player.bodyCollider then
    collidePlayer(player, x, y)
    return
  end
  if shape == player2.footCollider or shape == player2.bodyCollider  then
    collidePlayer(player2, x, y)
    return
  end
end

function collidePlayer(player, x, y)
  if y < 0 then
    player.verticalState = "grounded"
  elseif y > 0 then
    player.verticalState = "hit_ceiling"
  end

  if x > 0 then -- hit wall on left
  elseif x < 0  then -- hit wall on right
  end

  -- finally, resolve the collision
  local rx = 0
  local ry = 0
  if x > 0 then
    rx = 1
  elseif x < 0 then
    rx = -1
  end

  -- only move above a certain tolerance
  if y > 3 then
    ry = 0
  elseif x < -3 then
    ry = 0 
  end

  player:move(x + rx,y + ry)
end

function love.update(dt)

  level.collider:update(dt)

  player:handleInput()
  player:update(dt)

  player2:handleInput()
  player2:update(dt)

  horizontalState:SetText(player.movementState)
  verticalState:SetText(player.verticalState)

  loveframes.update(dt)
end

function drawHUD()
  local width = camera.width
  local height = camera.height
  local r,g,b,a = love.graphics.getColor()
  love.graphics.setColor(156, 171, 184,128)
  love.graphics.rectangle("fill", 0, camera.height-100, camera.width, camera.height)
  love.graphics.setColor(r,g,b,a)
end


function love.draw()

  -- push for translation
  love.graphics.push()
  camera:lookAt(player.x+player.w/2, player.y+player.h/2)

  if spiral ~= nil and coroutine.status(spiral) ~= 'dead' then
    print( "resuming spiral" )
    coroutine.resume(spiral)
  end

  level:draw(camera.world_x, camera.world_y, 1, 50)
  player:draw(player.x, player.y)
  player2:draw(player2.x, player2.y)
  level:drawFg()
  love.graphics.pop()

  -- overlay with GUI after pop
  loveframes.draw()
  drawHUD()
  love.graphics.print( "camera rotation: " .. camera.rotation, 10, camera.height-80)
  love.graphics.print( "camera scale: " .. camera.scale, 10, camera.height-60)
  love.graphics.print( "vertical state: " .. player.verticalState, 10, camera.height-40)
  love.graphics.print( "movement state: " .. player.movementState, 10, camera.height-20)

end

function relativeScale(s)
  if camera.scale + s > 0.1 and camera.scale+s < 5 then
    camera.scale = camera.scale + s
  end
end

function love.keypressed(k)
  print ("key pressed", k)
  if k == 'escape' or k == 'q' then
    love.event.quit()
  end
  if k == '=' then
    relativeScale(0.1)
  end
  if k == '-' then
    relativeScale(-0.1)
  end
  if k == '[' then
    camera.rotation = camera.rotation + 0.1
  elseif k == ']' then 
    camera.rotation = camera.rotation - 0.1
  end
  if k == 'g' then
    -- start a coroutine which zooms and rotates the camera, resetting at the original state
    spiral = effects.spiral(camera, 0, 2, 0.2)
  end

  loveframes.keypressed(k, unicode)
end

function love.keyreleased(key)
  loveframes.keyreleased(key)
end
function love.mousepressed(x, y, button)
  loveframes.mousepressed(x, y, button)
end

function love.mousereleased(x, y, button)
  loveframes.mousereleased(x, y, button)
end

function love.textinput(text)
  loveframes.textinput(text)
end
